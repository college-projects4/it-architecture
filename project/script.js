//displays message
function addToCart() {
    window.alert("Item added to cart");
}

//checks the user input matches the saved user
function userValidation() {
    var username = "example@mail.com";
    var password = "Password1";
    let entryPassword = document.forms["loginForm"]["password"].value;
    let entryUsername = document.forms["loginForm"]["username"].value;

    if (entryPassword == password && entryUsername == username) {
        localStorage.setItem("loggedin", true);
        window.alert("Login Successful");
        window.location.href = "index.html";
    } else {
        window.alert("Username or password incorrect");
    }
}

//checks that the sign up page is filled out correctly
function signUp() {
    let entryUsername = document.forms["signUpForm"]["username"].value;
    let entryPassword1 = document.forms["signUpForm"]["password1"].value;
    let entryPassword2 = document.forms["signUpForm"]["password2"].value;

    if (entryUsername != "" && entryPassword1 == entryPassword2 && entryPassword1 != "") {
        localStorage.setItem("loggedin", true);
        window.alert("Sign Up Successful")
        window.location.href = "index.html"
    } else if (entryPassword1 != entryPassword2) {
        window.alert("Passwords do not match")
    }
}

//displays message and returns to home page
function purchase() {
    window.alert("Purchase complete. Thank you!");
    window.location.href = "index.html";
}

